import { getTestDa } from '../services/test';
export default {
    state: {
        testData:null
    },
    reducers: {
      update: (state, payload) => ({ ...state, ...payload }),
    },
    effects: dispatch => ({
      async getTestData(_,{test}) {
        const data = await getTestDa();
        await this.update({ testData: data.data});    
      },
      
    }),
  };