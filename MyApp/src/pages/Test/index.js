import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { P } from '@uiw/react-native';



const BottomTabs = createBottomTabNavigator();

class Test extends Component {
  componentDidMount(){
    this.props.getTestData()
  }
  render() {
    const {testData}  = this.props
    return (
     <View>
       <P style={styles.description}>{testData.nikename}</P>
     </View>
    );
  }
}

export default connect(
  ({ test }) => ({
   testData:test.testData
  }),
    ({ test }) => ({
      getTestData: test.getTestData,
    }),
  )(Test);
  
  const styles = StyleSheet.create({
    description: {
      color: '#0000FF',
      fontSize: 12,
      marginBottom: 0,
      fontWeight: '200',
    },
  });